<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css">
    <link rel="stylesheet" href="assets/css/style.min.css">
</head>
<body>
<header class="feature-image">
<h1>MAR A CIELO</h1>
</header>
<main>
    <div class="content">
        <div class="bg-brand-dark">
            <div class="container">
                <div class="row">
                    <div class="col-md-11 mx-auto text-center py-6">
                        <h2>BAJA CULTURE IN CONTEMPORARY DESIGN</h2>
                        <h3>EXPRESSING THE AUTHENTICITY OF CABO DEL SOL</h3>
                        <p>Mar a Cielo is Cabo del Sol’s newest real estate offering. The architecture, interiors, and landscapes of Mar a Cielo interpret Cabo del Sol&nbsp;through a modern lens. From the outside in, every residence reflects the warm, luxurious climate and natural beauty of the sun-drenched Baja peninsula. Unlike many Los Cabos properties, Mar a Cielo is not branded. There are no restrictions on owner usage or prescribed property management; homeowners are free to use and rent their homes however they wish.</p>
                    </div>
                </div>
            </div>
        </div><!--.bg-brand-dark-->
    </div>
    <div class="full-img">
        <img class="img-fluid" src="assets/images/MCDS_Web_Residences_livingroom.jpg" alt="Living Room">
    </div>
    <div class="content">
        <div class="bg-brand-dark">
            <div class="container">
                <div class="row">
                    <div class="col-md-11 mx-auto text-center py-6">
                        <h2>COME HOME TO UNFORGETTABLE MOMENTS</h2>
                        <h3>INTIMATE REAL ESTATE, FULLY-FURNISHED FOR MODERN LIVING</h3>
                        <p>Mar a Cielo is a special release of 36 modern homes. The two- and three-bedroom residences feature open floorplans and oversized windows framing views of the ever-changing sea. Generous outdoor spaces invite relaxation, and family gatherings that become lasting memories. Days unfold in harmony with nature. Thoughtful design brings the colors, textures, and traditions of Baja into each and every home.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="full-img">
        <img class="img-fluid" src="assets/images/MCDS_Web_Residences_livingroom.jpg" alt="Balcony">
    </div>
    <div class="container-fluid pr-0 bg-brand-light py-3">
        <div class="row mr-0">
            <div class="col-md-6 text-center center-content px-5">
                <img class="img-fluid wind" src="assets/images/MCDS_Web_Residences_wind.svg" alt="Wind">
                <h3 class="mb-2 mt-5">SEAMLESS TRANSITIONS FROM OUTSIDE TO INSIDE</h3>
                <p>The Baja lifestyle makes few distinctions between the outdoors and indoors. Mar a Cielo embraces this attitude, with spaces designed to maximize ocean views, warm sea breezes, and lush plant life. Each residence extends seamlessly into open-air patios, terraces, and balconies that encourage a slower pace – and time to absorb the beauty that enfolds Cabo del Sol. Throughout the property, nature takes priority. Flourishing gardens, courtyards, lanais, and decks blur the lines between the built and natural environment.</p>
            </div>
            <div class="full-img col-md-6 pr-0">
                <img class="img-fluid" src="assets/images/MCDS_Web_Residences_terrace3.jpg" alt="Terrace">
            </div>
        </div>
    </div>
    <div class="content">
        <div class="bg-brand-dark">
            <div class="container">
                <div class="row">
                    <div class="col-md-11 mx-auto text-center py-6">
                        <h2>THE RESIDENCES</h2>
                        <h3>BORN FROM LAND AND CULTURE</h3>
                        <p>Mar a Cielo’s real estate offerings  embody the warmth and luxury of Los Cabos itself. They represent the very best of this land: its stunning natural beauty, diverse landscape, rich culture, and relaxed, generous way of life. The design interprets a storied past with a modern sensibility – and gives homeowners access to every amenity they could desire. The property also occupies a coveted location behind Hole #5 of Cabo del Sol Ocean Course and the forthcoming Beach Club.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div id="slider">
            <div>
                <img class="img-fluid" src="assets/images/MCDS_Web_Residences_siteplan.svg" alt="SitePlan">
            </div>
            <div>
                <img class="img-fluid"  src="assets/images/MCDS_Web_Residences_view02b.jpg" alt="View">
            </div>
            <div>
                <img class="img-fluid" src="assets/images/MCDS_Web_Residences_view03c.jpg" alt="View">
            </div>
        </div><!--#slider-->
    </div>
    <div class="content bg-brand-light py-6 text-center">
        <div class="container specifications">
        <h2>FOR EVERY HOME</h2>
        <p>For every spacious bedroom, there is a bathroom to match. Each home includes a den, equipment storage area for stashing sports gear, and a generous, corner-wrapping terrace. Airy, modern kitchens open up to living and dining areas ideal for gathering with family and friends.</p>
            <div class="row">
                <div class="col-md-6 text-center">
                    <h4>2-BEDROOM</h4>
                    <h6>2,440 TOTAL SF</h6>
                    <p>
                        <span>Interior 1,699 SF <span class="sep"> | </span> Exterior 741 SF</span>
                    </p>
                </div>
                <div class="col-md-6 text-center">
                    <h4>3-BEDROOM</h4>
                    <h6>3,253 TOTAL SF</h6>
                    <p>
                        <span>Interior 2,118 SF <span class="sep"> | </span> Exterior 1,135 SF</span>
                    </p>
                </div>
            </div>
        </div><!--.specifications-->
    </div>
    <div class="content">
        <img class="img-fluid w-100" src="assets/images/form.jpg" alt="Form">
    </div>
</main>
<footer>
    <div class="content">
        <img class="img-fluid w-100" src="assets/images/footer.jpg" alt="Footer">
    </div>
</footer>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
<script>
    $.getJSON('https://www.maracielo.com/wp-json/wp/v2/pages/46', function (data) {
        console.log(data);
        $('title').text(data.title.rendered);
    });

    $.getJSON('https://www.maracielo.com/wp-json/wp/v2/media?parent=46', function (data) {
        console.log(data);
    });

    $('#slider').slick({
        arrows: true
    });
</script>
</html>